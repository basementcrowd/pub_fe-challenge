/** @type {import('ts-jest').JestConfigWithTsJest} **/
module.exports = {
  transform: {
    "^.+.tsx?$": ["ts-jest",{}],
  },
  verbose: true,
  moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
  testEnvironment: "jsdom",
  setupFilesAfterEnv: ["<rootDir>/src/testUtils/setupTests.ts"],
  moduleNameMapper: {
    "\\.css$": "<rootDir>/src/testUtils/styleMock.ts"
  }
};