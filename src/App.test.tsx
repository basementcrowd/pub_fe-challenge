import {render, screen} from '@testing-library/react'
import App from './App'

test("The app component renders the header \"Hello, world\"", async () => {
  render(<App />)

  await screen.getByRole("heading")

  expect(screen.getByRole('heading')).toHaveTextContent("Hello, world")
})